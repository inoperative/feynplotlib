feynplotlib
===========

A script for drawing Feynman diagrams using the XKCD style in matplotlib.

Currently only supports FeynArts output, but it should be 
straightforward to add support for axodraw output.

To use, generate the diagrams for the process of interest using 
FeynArts (http://www.feynarts.de/) and output in TeX format, e.g.:

`diagrams = Paint[topologies];`
`Export["diagrams.tex", diagrams];`

Then just run the feynplotlib.py script on the TeX file:

`python feynplotlib.py diagrams.tex`

This will generate separate png files for each diagram in the folder 
the script is run from. If you want a different file format you can
request any format supported by MPL with a second argument.

These can be easily combined into a single file 
(the way FeynArts works by default) by using ImageMagick:

`montage -geometry 900x500+0+0 *.png out.png`

(The output format for montage can be changed by just changing the 
output file extension.)

Contributors: Karl Nordstrom (k.nordstrom.1@research.gla.ac.uk)
