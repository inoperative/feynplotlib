
#! /usr/bin/env python

import sys
import os
import numpy as np
import matplotlib.pyplot as plt

def points_in_circle(r, xc, yc, ang1=0., ang2=2*np.pi, rotate=0., n=100):
  return [[(np.cos(abs(ang2-ang1)/n*x + ang1 + rotate)*r+xc) for x in xrange(0,n)], [(np.sin(abs(ang2-ang1)/n*x + ang1 + rotate)*r+yc) for x in xrange(0,n)]]
def points_in_circle_lower(r, xc, yc, ang1=0., ang2=2*np.pi, rotate=0., n=100):
  return [[(np.cos(abs(ang2-ang1)/n*x + ang2 + rotate)*r+xc) for x in xrange(0,n)], [(np.sin(abs(ang2-ang1)/n*x + ang2 + rotate)*r+yc) for x in xrange(0,n)]]
def points_in_circle_upper(r, xc, yc, ang1=0., ang2=2*np.pi, rotate=0., n=100):
  return [[(np.cos(abs(ang2-ang1)/n*x + ang1 + rotate)*r+xc) for x in xrange(0,n)], [(np.sin(abs(ang2-ang1)/n*x + ang1 + rotate)*r+yc) for x in xrange(0,n)]]

def points_in_full_circle(r, xc, yc, angle, n=100):
  return [[(np.cos((2*np.pi)/n*x + angle)*r+xc) for x in xrange(0,n)], [(np.sin((2*np.pi)/n*x + angle)*r+yc) for x in xrange(0,n)]]

def gluon_prop_semicircle(x1,x2,y1,y2,angle):
  if abs(x2-x1) < 0.00001:
    if (y2-y1) > 0.:
      theta = np.pi/2.
    if (y2-y1) < 0.:
      theta = - np.pi/2.
  else:
    theta = np.arctan((y2-y1)/(x2-x1))

  length = np.sqrt((x2-x1)**2 + (y2-y1)**2)
  length_orig = length

  d = length / 2.

  cent_theta = [length/2,  0]
  r = d*abs(angle)
  opening_angle = np.pi
  length_angle = opening_angle * r

  length = length_angle

  thetarange = (5*int(length) - 1) /2 * np.pi
  t = np.linspace(3 * np.pi/2., thetarange, 100)
  xf = 0.4 * np.sin(t)
  yf = 0.4 *  np.cos(t)
  x = np.zeros(100)
  y = np.zeros(100)

  ## HACK!! since FeynArts output for vertical loops is weird
  if abs(theta + np.pi/2) < 0.0001:
    angle = -angle

  if angle > 0:
    circle = points_in_circle_upper(r,cent_theta[0],cent_theta[1],0,np.pi)
  if angle < 0:
    circle = points_in_circle_lower(r,cent_theta[0],cent_theta[1],0,np.pi)
  for i in range (0,100):
    circle[0][i] = circle[0][i] - d*(1-abs(angle))
    circle[0][i] = circle[0][i] * (length_orig/(2*r))
    circle[0][i] = circle[0][i] + xf[i] + (100-i)*0.4/100
    circle[1][i] = circle[1][i] + yf[i]
    if x1 <= x2:
      x[i] = circle[0][i]*np.cos(theta) - circle[1][i] * np.sin(theta) + x1
      y[i] = circle[0][i]*np.sin(theta) + circle[1][i] * np.cos(theta) + y1
    if x2 < x1:
      x[i] = circle[0][i]*np.cos(theta) - circle[1][i] * np.sin(theta) + x2
      y[i] = circle[0][i]*np.sin(theta) + circle[1][i] * np.cos(theta) + y2

  plt.plot(x,y,color = 'black')

def gluon_prop_circle(length,circle):
  thetarange = (4*int(length) - 2.9) /2 * np.pi
  t = np.linspace(3 * np.pi/2., thetarange, 200)
  xf = 0.4 * np.sin(t)
  yf = 0.4 *  np.cos(t)
  x = np.zeros(200)
  y = np.zeros(200)

  for i in range (0,200):
    y[i] = circle[1][i] + yf[i]
    x[i] = circle[0][i] + xf[i] + 0.3 -i/200. * 0.7

  plt.plot(x,y,color = 'black',zorder=0)

def vector_prop_semicircle(x1,x2,y1,y2,angle):
  if abs(x2-x1) < 0.00001:
    if (y2-y1) > 0.:
      theta = np.pi/2.
    if (y2-y1) < 0.:
      theta = - np.pi/2.
  else:
    theta = np.arctan((y2-y1)/(x2-x1))

  length = np.sqrt((x2-x1)**2 + (y2-y1)**2)
  length_orig = length

  d = length / 2.

  cent_theta = [length/2,  0]
  r = d*abs(angle)
  opening_angle = np.pi
  length_angle = opening_angle * r

  length = length_angle

  x = np.zeros(100)
  y = np.zeros(100)

  n_l = int(length*1.5)
  xf = np.linspace(0, length, 100)
  yf = 0.3*np.sin((n_l) * np.pi * (xf/length) )

  ## HACK!! since FeynArts output for vertical loops is weird
  if  abs(theta + np.pi/2) < 0.0001:
    angle = -angle

  for i in range (0,100):
    if angle > 0:
      circle = points_in_circle_upper(r+yf[i],cent_theta[0],cent_theta[1],0,np.pi)
    if angle < 0:
      circle = points_in_circle_lower(r+yf[i],cent_theta[0],cent_theta[1],0,np.pi)

    circle[0][i] = circle[0][i] - d*(1-abs(angle))
    circle[0][i] = circle[0][i] * (length_orig/(2*r))
    if x1 <= x2:
      x[i] = circle[0][i]*np.cos(theta) - circle[1][i] * np.sin(theta) + x1
      y[i] = circle[0][i]*np.sin(theta) + circle[1][i] * np.cos(theta) + y1
    if x2 < x1:
      x[i] = circle[0][i]*np.cos(theta) - circle[1][i] * np.sin(theta) + x2
      y[i] = circle[0][i]*np.sin(theta) + circle[1][i] * np.cos(theta) + y2

  plt.plot(x,y,color = 'black')

def vector_prop_circle(length,r,centx,centy,angle,circle):
  n_l = int(length/1.2)
  xf = np.linspace(0, length, 200)
  yf = np.zeros(200)
  x = np.zeros(200)
  y = np.zeros(200)
    # centre of circles
#      plt.plot(cent[0] + x1,cent[1] + y1,color = 'black',ms=20,marker='x')
  x[0] = circle[0][0]
  y[0] = circle[1][0]
  for i in range (1,200):
    yf[i] = 0.3*np.sin((n_l) * np.pi * (xf[i]/length) )
    circle_tmp = points_in_full_circle(r+yf[i],centx,centy,angle,200)
    y[i] = circle_tmp[1][i]
    x[i] = circle_tmp[0][i]
  plt.plot(x,y,color = 'black')

def prop(line):
  parts = line.split(')')
  parts[0] = parts[0].split('(')[1]
  parts[1] = parts[1][1:]
  parts[2] = parts[2][1:]
  parts3 = parts[3].split('}')
  linestyle = parts3[0][1:]
  ls = None
  if linestyle == "ScalarDash":
    ls = '--'
  if linestyle == "Straight":
    ls = '-'
  if linestyle == "Sine":
    ls = 'vector'
  if linestyle == "Cycles":
    ls = 'qcd'
  if linestyle == "GhostDash":
    ls = ':'
  arrow = int(parts3[1][1:])
  orig = parts[0].split(',')
  end = parts[1].split(',')
  angle = parts[2].split(',')
  x1 = float(orig[0])
  x2 = float(end[0])
  y1 = float(orig[1])
  y2 = float(end[1])
  if abs(x2-x1) < 0.00001:
    if (y2-y1) > 0.:
      theta = np.pi/2.
    if (y2-y1) < 0.:
      theta = - np.pi/2.
  else:
    theta = np.arctan((y2-y1)/(x2-x1))

  if angle[1] == '':
    angle[1] = '0.'
  if abs(float(angle[1])) < 0.00001:
    length = np.sqrt((x2-x1)**2 + (y2-y1)**2)
    length_orig = np.sqrt((x2-x1)**2 + (y2-y1)**2)
    x_pre = np.linspace(0, length, 100)
    y_offset = np.zeros(100)
    angle_used = False
    if (abs(float(angle[0])) > 0.00001 ):
      rat = - float(angle[0])
      angle_used = True
      d = length / 2.
      h = rat * d
      r = abs( (d**2 - h**2)/(2*h) + h )
      opening_angle = 2* np.arcsin( (length/2.) / r)
      length_angle = opening_angle * r
      cent = [d, - (d**2 - h**2)/(2*h)]
      cent_theta = [d*np.cos(theta) + np.sin(theta) * (d**2 - h**2)/(2*h),  d*np.sin(theta) - np.cos(theta) * (d**2 - h**2)/(2*h)]
      for i in range (0,100):
        y_offset[i] = ( np.copysign(np.sqrt(r**2-(x_pre[i]-cent[0])**2),rat ) + cent[1])

    if ls == None:
      line=plt.plot([orig[0],end[0]],[orig[1],end[1]],'k-', zorder=1, dash_capstyle='butt',alpha=1.0)

    elif ls == 'vector':
      if angle_used:
        vector_prop_semicircle(x1,x2,y1,y2,float(angle[0]))
      else:
        xf = np.linspace(0, length, 100)

        n_l = int(length*1.2)
        yf = np.zeros(100)
        x = np.zeros(100)
        y = np.zeros(100)
        for i in range (1,100):
          y[i] = 0.5*np.sin((n_l+1) * np.pi * (xf[i]/length) )
          x[i] = xf[i]
        if x1 <= x2:
          xp = x*np.cos(theta) - y * np.sin(theta) + x1
          yp = x*np.sin(theta) + y * np.cos(theta) + y1
        if x2 < x1:
          xp = x*np.cos(theta) - y * np.sin(theta) + x2
          yp = x*np.sin(theta) + y * np.cos(theta) + y2
        plt.plot(xp,yp,color = 'black',zorder=0)
    elif ls == 'qcd':

      if angle_used:
        gluon_prop_semicircle(x1,x2,y1,y2,float(angle[0]))

      else:
        thetarange = (4*int(length) - 2.9) /2 * np.pi
        t = np.linspace(3 * np.pi/2., thetarange, 100)
        xf = 0.4 * np.sin(t)
        yf = 0.4 *  np.cos(t)
        x = np.zeros(100)
        y = np.zeros(100)

        for i in range (0,100):
          xf[i] = xf[i] + i * length/100.

        # "renormalise" the length
        length_new = xf[99] - xf[0]
        for i in range (0,100):
          xf[i] = xf[i] * length/length_new
        for i in range (0,100):
          xf[i] = xf[i] + 0.3

        for i in range (0,100):
          x[i] = xf[i]
          y[i] = yf[i]

        if x1 <= x2:
          xp = x*np.cos(theta) - y * np.sin(theta) + x1
          yp = x*np.sin(theta) + y * np.cos(theta) + y1
        if x2 < x1:
          xp = x*np.cos(theta) - y * np.sin(theta) + x2
          yp = x*np.sin(theta) + y * np.cos(theta) + y2
        plt.plot(xp,yp,color = 'black')
    elif ls == '-' or ls == '--' or ls == ':':
      x = np.linspace(0, length, 100)
      y = y_offset
      if abs(x2-x1) < 0.00001:
        if (y2-y1) > 0.:
          theta = np.pi/2.
        if (y2-y1) < 0.:
          theta = - np.pi/2.
      else:
        theta = np.arctan((y2-y1)/(x2-x1))
      if x1 <= x2:
        xp = x*np.cos(theta) - y * np.sin(theta) + x1
        yp = x*np.sin(theta) + y * np.cos(theta) + y1
      elif x2 < x1:
        xp = x*np.cos(theta) - y * np.sin(theta) + x2
        yp = x*np.sin(theta) + y * np.cos(theta) + y2
      plt.plot(xp,yp,color = 'black',ls=ls)
      # this is just ridiculously ugly but works...
    if arrow == 1:
      plt.annotate('', xy=(-np.sin(theta)*y_offset[49]+x1+0.48*(x2-x1) + (x2-x1)/length/2,  np.cos(theta)*y_offset[49]+y1+0.47*(y2-y1)+(y2-y1)/length/2), xycoords='data',
          xytext=(-np.sin(theta)*y_offset[49]+x1+0.48*(x2-x1), np.cos(theta)*y_offset[49]+y1+0.47*(y2-y1)), textcoords='data',
          arrowprops=dict( headwidth=15., frac=1., ec='black', fc='black'))
    if arrow == -1:
      plt.annotate('', xy=(-np.sin(theta)*y_offset[49]+x1+0.46*(x2-x1),  np.cos(theta)*y_offset[49]+y1+0.47*(y2-y1)), xycoords='data',
          xytext=(-np.sin(theta)*y_offset[49]+x1+0.46*(x2-x1) + (x2-x1)/length/2, np.cos(theta)*y_offset[49]++y1+0.47*(y2-y1)+(y2-y1)/length/2), textcoords='data',
          arrowprops=dict( headwidth=15., frac=1., ec='black', fc='black'))
  else:
    centx = float(angle[0]) - (float(angle[0]) - x1)/2
    centy = float(angle[1]) - (float(angle[1]) - y1)/2
    if abs(centx-x1) < 0.00001:
      if (centy-y1) < 0.:
        angle = np.pi/2.
      if (centy-y1) > 0.:
        angle = - np.pi/2.
    r = np.sqrt( (centx-x1)**2 + (centy-y1)**2)
    length = 2*np.pi *r
    circle = points_in_full_circle(r,centx,centy,angle,200)
    if ls == 'vector':
      vector_prop_circle(length,r,centx,centy,angle,circle)
    elif ls == 'qcd':
      gluon_prop_circle(length,circle)
    elif ls == '-' or ls == '--' or ls == ':':
      plt.plot(circle[0],circle[1],color = 'black',ls=ls)
    if arrow == 1:
      plt.annotate('', xy=(x1+0.48*(x2-x1) + (x2-x1)/length/2,  circle[1][49]+y1+0.47*(y2-y1)+(y2-y1)/length/2), xycoords='data',
          xytext=(x1+0.48*(x2-x1), circle[1][49]+y1+0.47*(y2-y1)), textcoords='data',
          arrowprops=dict( headwidth=15., frac=1., ec='black', fc='black'))
    if arrow == -1:
      plt.annotate('', xy=(x1+0.46*(x2-x1) ,  circle[1][49]+y1+0.47*(y2-y1)), xycoords='data',
          xytext=(x1+0.46*(x2-x1) + (x2-x1)/length/2, circle[1][49]+y1+0.47*(y2-y1)+(y2-y1)/length/2), textcoords='data',
          arrowprops=dict( headwidth=15., frac=1., ec='black', fc='black'))

def label(line):
  parts = line.split(')')
  parts[0] = parts[0].split('(')[1]
  loc = parts[0].split(',')
  text = parts[1].split('{')[1]
  text = text[:-3]
  text = text[1:]
  if str(text) == "\gamma":
    plt.text(loc[0],loc[1], "P", size = 20)
  elif str(text) == "u_-":
    plt.text(loc[0],loc[1], "U-", size = 20)
  elif str(text) == "u_+":
    plt.text(loc[0],loc[1], "U+", size = 20)
  elif str(text) == "u_Z":
    plt.text(loc[0],loc[1], "UZ", size = 20)
  elif str(text) == "u_G":
    plt.text(loc[0],loc[1], "UG", size = 20)
  else:
    plt.text(loc[0],loc[1], str(text), size = 20)

def vert(line):
  parts = line.split(')')
  parts[0] = parts[0].split('(')[1]
  cto = parts[1][1:-2]
  if int(cto) > 0:
    loc = parts[0].split(',')
    coords = [[loc[0],loc[1]]]
    # need to trick the margin calculator in order
    # to stop markers from getting cut off
    coords1 = [[loc[0],float(loc[1])+0.2]]
    coords2 = [[loc[0],float(loc[1])-0.2]]
    coords3 = [[float(loc[0])+0.2,loc[1]]]
    coords4 = [[float(loc[0])-0.2,loc[1]]]
    plt.plot(*zip(*coords), marker='x', color='black', ls='',ms=20)
    plt.plot(*zip(*coords1), marker='', color='black', ls='',ms=0)
    plt.plot(*zip(*coords2), marker='', color='black', ls='',ms=0)
    plt.plot(*zip(*coords3), marker='', color='black', ls='',ms=0)
    plt.plot(*zip(*coords4), marker='', color='black', ls='',ms=0)


def main():
  if len(sys.argv) < 2:
    print "This script generates Feynman diagrams in XKCD style"
    print "using matplotlib from a FeynArts TeX file. Usage: "
    print ""
    print "python feynplotlib.py diagrams.tex"
    print ""
    print "An optional second argument can be used to set the"
    print "output format to any supported by MPL (default: png)."
    sys.exit()

  filename = sys.argv[1]
  if len(sys.argv) > 2:
    format = str(sys.argv[2])
  else:
    format = "png"
  plt.xkcd()

  if os.path.isfile(filename):
    with open(filename) as f:
      name = None
      for line in f.readlines():
        ## Save diagram (without attempting to save empty ones)
        if "\end{feynartspicture}" in line:
          if name != None:
            plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0,
                        hspace = 0, wspace = 0)
            plt.margins(0,0)
            plt.gca().xaxis.set_major_locator(plt.NullLocator())
            plt.gca().yaxis.set_major_locator(plt.NullLocator())
            plt.savefig(name+"."+format, format=format, dpi=300,bbox_inches='tight', pad_inches=0)
          name = None
        if "\FADiagram" in line:
          if name != None:
            plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0,
                        hspace = 0, wspace = 0)
            plt.margins(0,0)
            plt.gca().xaxis.set_major_locator(plt.NullLocator())
            plt.gca().yaxis.set_major_locator(plt.NullLocator())
            plt.savefig(name+"."+format, format=format, dpi=300,bbox_inches='tight', pad_inches=0)
          name = line.split('{')[1][:-2]
          plt.clf()
          ax1 = plt.axes(frameon=False)
          ax1.axes.get_yaxis().set_visible(False)
          ax1.axes.get_xaxis().set_visible(False)
          plt.gca().set_aspect('equal')
        ## Draw the constituents of the diagrams
        if "\FAProp" in line:
          prop(line)
        if "\FALabel" in line:
          label(line)
        if "\FAVert" in line:
          vert(line)
  else:
    print "Given input file does not exist."
    sys.exit()
  sys.exit()


if __name__ == "__main__":
  main()
